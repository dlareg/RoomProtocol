#ifndef ROOMFRAME_H
#define ROOMFRAME_H

#include <QByteArray>
#include <QObject>

class RoomMessage
{
public:
    explicit RoomMessage(QByteArray content = QByteArray());
    RoomMessage(const RoomMessage& origin);
    explicit RoomMessage(uint8_t verb, uint8_t noun, QByteArray args = QByteArray());

    bool isValid() const;
    bool isComplete() const;

    int addressCount() const;
    QByteArray header() const;
    uint8_t popAddress() ;
    void pushAddress(uint8_t byte);

    uint8_t verb() const;
    uint8_t noun() const;
    QByteArray args() const;

    QByteArray payload() const;
    QByteArray bytes() const;

private:
    QByteArray m_header;    //Contains addressing
    QByteArray m_payload;
};

Q_DECLARE_METATYPE(RoomMessage)

#endif // ROOMFRAME_H
