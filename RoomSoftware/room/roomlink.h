#ifndef ROOMLINK_H
#define ROOMLINK_H

#include "roommessage.h"

#include <QIODevice>
#include <QObject>
#include <QPointer>
#include <QTimer>

class RoomLink : public QObject
{
    Q_OBJECT
public:
    explicit RoomLink(QObject *parent = nullptr);
    void setDevice(QIODevice* device);

    void transmitMsg(RoomMessage msg);

    void autoPing(int delayPing, int delayPong);

signals:
    void msgReceived(RoomMessage msg);
    void pingTimeout();

private slots:
    void sendPing();
    void onPongReceived();

private:
    void readDeviceData();
    QByteArray buildByte(char c);

    QByteArray m_rxFrameBytes;
    bool m_isNextByteEscaped;

    QPointer<QIODevice> m_device;

    QTimer* m_pingTimer = nullptr;
    QTimer* m_pongTimer = nullptr;
};

#endif // ROOMLINK_H
