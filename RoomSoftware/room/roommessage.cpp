#include "roommessage.h"
#include "roomprotocol.h"

RoomMessage::RoomMessage(QByteArray content)
{
    QList<QByteArray> parts = content.split(ROOM_BYTE_HEADER_SEP);
    m_payload = parts.takeLast();
    if(!parts.isEmpty()) m_header = parts.first();
}

RoomMessage::RoomMessage(const RoomMessage &origin)
{
    m_payload = origin.m_payload;
    m_header = origin.m_header;
}

RoomMessage::RoomMessage(uint8_t verb, uint8_t noun, QByteArray args)
{
    m_payload.append(verb);
    m_payload.append(noun);
    m_payload.append(args);
}

bool RoomMessage::isValid() const
{
    return m_payload.size() >= 1;
}

bool RoomMessage::isComplete() const
{
    return m_payload.size() >= ROOM_MIN_LENGTH;
}

int RoomMessage::addressCount() const
{
    return m_header.size();
}

QByteArray RoomMessage::header() const
{
    return m_header;
}

uint8_t RoomMessage::popAddress()
{
    uint8_t a = m_header.at(0);
    m_header.remove(0,1);
    return a;
}

void RoomMessage::pushAddress(uint8_t byte)
{
    m_header.prepend(byte);
}

uint8_t RoomMessage::verb() const
{
    return m_payload[ROOM_FRAME_VERB_INDEX];
}

uint8_t RoomMessage::noun() const
{
    return m_payload[ROOM_FRAME_NOUN_INDEX];
}


QByteArray RoomMessage::args() const
{
    return m_payload.right(m_payload.length() - ROOM_FRAME_ARGS_INDEX);
}

QByteArray RoomMessage::payload() const
{
    return m_payload;
}

QByteArray RoomMessage::bytes() const
{
    QByteArray bytes;
    if(!m_header.isEmpty())
    {
        bytes.append(m_header);
        bytes.append(ROOM_BYTE_HEADER_SEP);
    }
    bytes.append(m_payload);
    return bytes;
}
