#include "roomclient.h"

#include "roomprotocol.h"
#include "roomnetworklink.h"
#include "roommodel.h"
#include "roomtranslator.h"
#include "multiplexer.h"

#include <QTime>

RoomClient::RoomClient(QObject *parent) :
    QObject(parent),
    m_model(new RoomModel(this)),
    m_translator(new RoomTranslator(m_model,this)),
    m_netLink(nullptr),
    m_multiplexer(nullptr)
{
    connect(m_model, &RoomModel::altered, this, &RoomClient::roomAltered);
    connect(m_model, &RoomModel::altered, [=](RoomAlteration alteration)
    {
        logMessage(alteration.logString());
    });
}

void RoomClient::connectToNetworkHost(QString hostName, quint16 port)
{
    setConnected(false);
    if(m_multiplexer) delete m_multiplexer;
    if(m_netLink) delete m_netLink;
    m_netLink = new RoomNetworkLink(hostName, (port != 0) ? port : ROOM_TCP_DEFAULT_PORT, this);

    connect(m_translator, &RoomTranslator::msgToEndpoint,
            m_netLink, &RoomNetworkLink::transmitMsg);
    connect(m_netLink, &RoomNetworkLink::msgReceived,
            m_translator, &RoomTranslator::msgFromEndpoint);
    connect(m_netLink, &RoomNetworkLink::connectedChanged,
            this, &RoomClient::connectedChanged);
    connect(m_netLink, &RoomNetworkLink::connectedChanged,
            this, &RoomClient::onNetworkConnectedChanged);


    logMessage(QStringLiteral("Remote host is ") + m_netLink->hostname() + ":" + QString::number(m_netLink->port()));
    m_netLink->startConnection();
}

void RoomClient::connectToLocalEndpoints()
{
    setConnected(false);
    if(m_netLink) delete m_netLink;
    if(m_multiplexer) delete m_multiplexer;
    m_multiplexer = new Multiplexer(this);

    connect(m_translator, &RoomTranslator::msgToEndpoint,
            m_multiplexer, &Multiplexer::msgToEndpoint);
    connect(m_multiplexer, &Multiplexer::msgFromEndpoint,
            m_translator, &RoomTranslator::msgFromEndpoint);

    m_multiplexer->doEndpointsDiscovery();

    logMessage(QStringLiteral("Connected to local endpoints"));
    setConnected(true);
}

bool RoomClient::connected() const
{
    return m_connected;
}

RoomModel *RoomClient::getModel()
{
    return m_model;
}

void RoomClient::onNetworkConnectedChanged(bool connected)
{
    RoomNetworkLink* sourceLink = qobject_cast<RoomNetworkLink*>(sender());
    if(!sourceLink) return;

    setConnected(connected);

    if(connected)
    {
        logMessage(QString("Connected"));
        m_model->clearEndpoints();
        QMetaObject::invokeMethod(m_translator, "requestDiscovery", Qt::QueuedConnection);
    }
    else
    {
        logMessage(QString("Disconnected"));
    }
}

void RoomClient::setConnected(bool connected)
{
    if(connected != m_connected)
    {
        m_connected = connected;
        emit connectedChanged(m_connected);
    }
}

void RoomClient::logMessage(QString msg)
{
    QString timestamp = QStringLiteral("[%1] ").arg(QTime::currentTime().toString("hh:mm:ss.zzz"));
    emit logOut(timestamp + msg);
}

