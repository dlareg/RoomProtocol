#include "roommodel.h"
#include "endpointitem.h"
#include "roomprotocol.h"
#include <QDebug>

#define NO_PARENT   (-1)

RoomModel::RoomModel(QObject *parent) :
    QAbstractItemModel(parent)
{

}



QHash<int, QByteArray> RoomModel::roleNames() const
{
    return QHash<int, QByteArray>(
    {
        { Qt::DisplayRole   , "label"               },
        { ValueRole         , "value"               },
        { TypeRole          , "type"                },
        { CapsRole          , "caps"                },
        { LengthRole        , "length"              },
        { isEndpointRole    , "isEndpoint"          },
        { LocationRole      , "location"            },
        { ResetRole         , "reset"               }
    });
}

QModelIndex RoomModel::index(int row, int column, const QModelIndex &parent) const
{
    if(!hasIndex(row, column, parent)) return QModelIndex();

    //Internal ID :
    // NO_PARENT for first level items ( = endpoints )
    // endpoint id for second level items (= resources )
    return createIndex(row, column, parent.isValid() ? parent.row() : NO_PARENT);
}

QModelIndex RoomModel::parent(const QModelIndex &child) const
{
    if(!child.isValid()) return QModelIndex();
    int parentRow = child.internalId();
    return (parentRow == NO_PARENT) ? QModelIndex() : createIndex(parentRow, 0, NO_PARENT);
}

int RoomModel::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return 2;
}

int RoomModel::rowCount(const QModelIndex &parent) const
{
    if(!parent.isValid()) return m_endpoints.size();
    else if(parent.parent().isValid()) return 0;
    else return  m_endpoints.at(parent.row())->countResources();
}

QVariant RoomModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) return QVariant();

    int parentId = index.internalId();
    int row = index.row();

    //First level data = ENDPOINT
    if( parentId == NO_PARENT)
    {
        if(index.row() >= m_endpoints.size()) return QVariant();

        EndpointItem *endpoint = m_endpoints.at(row);

        switch(role)
        {
        case Qt::DisplayRole:
            return QVariant::fromValue(endpoint->name());
        case isEndpointRole:
            return QVariant::fromValue(true);
        case TypeRole:
            return QVariant::fromValue(ResourceItem::Type_None);
        case CapsRole:
            return QVariant::fromValue(ResourceItem::Cap_None);
        case LocationRole:
            return QVariant::fromValue(endpoint->location());
        default:
            return QVariant();
        }
    }

    //Second level data = RESOURCE
    else
    {
        if(index.row() >= m_endpoints.at(parentId)->countResources()) return QVariant();

        const ResourceItem * resource = m_endpoints.at(parentId)->getResource(row);

        switch(role)
        {
        case Qt::DisplayRole:
            return QVariant::fromValue(resource->label());
        case ValueRole:
            return QVariant::fromValue(resource->value());
        case TypeRole:
            return QVariant::fromValue(resource->type());
        case CapsRole:
            return QVariant::fromValue(resource->caps());
        case LengthRole:
            return QVariant::fromValue((int)resource->maxLength());
        case isEndpointRole:
            return QVariant::fromValue(false);
        default:
            return QVariant();
        }
    }

    return QVariant();
}


bool RoomModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid()) return false;

    int parentId = index.internalId();
    int row = index.row();

    //First level data = ENDPOINT
    if( parentId == NO_PARENT)
    {
        EndpointItem* endpoint = m_endpoints.at(row);
        if(!endpoint) return false;
        if(role != ResetRole) return false;
        emit requestEndpointReset(endpoint);
        return true;
    }

    else
    {
        EndpointItem *targetEndpoint = m_endpoints.at(parentId);
        if(index.row() >= targetEndpoint->countResources()) return false;
        if(role != ValueRole) return false;

        emit commandToSend(targetEndpoint,row,value);
        return true;
    }
}


void RoomModel::clearEndpoints()
{
    beginResetModel();
    qDeleteAll(m_endpoints);
    m_endpoints.clear();
    endResetModel();
}

void RoomModel:: insertEndpoint(uint8_t endpointId, EndpointItem *endpoint)
{
    int endpointRow = findEndpointRowWithId(endpointId);
    if(endpointRow >= 0 )
    {
        delete m_endpoints.at(endpointRow);
        m_endpoints[endpointRow] = endpoint;
        QModelIndex changedIndex = index(endpointRow,0);
        emit dataChanged(changedIndex, changedIndex);
    }
    else
    {
        int insertIndex = m_endpoints.size();
        beginInsertRows(QModelIndex(), insertIndex, insertIndex);
        m_endpoints.append(endpoint);
        endInsertRows();
    }

    for(int i = 0 ; i < endpoint->countResources() ; i++)
    {
        ResourceItem * resource = endpoint->getResource(i);
        emit altered(RoomAlteration(endpoint, resource));
    }
}

const ResourceItem *RoomModel::getEndpointResource(uint8_t endpointId, uint8_t resourceId)
{
    int endpointRow = findEndpointRowWithId(endpointId);
    if(endpointRow < 0 ) return nullptr;
    return m_endpoints.at(endpointRow)->getResource(resourceId);
}

void RoomModel::setEndpointResourceValue(uint8_t endpointId, uint8_t resourceId, QVariant value)
{
    int endpointRow = findEndpointRowWithId(endpointId);

    if(endpointRow < 0 ) return;

    ResourceItem* targetRes = m_endpoints.at(endpointRow)->getResource(resourceId);

//    qDebug() << "res" << targetRes->label() <<" val" << value;

    if((!targetRes) || (targetRes->value() == value)) return;
    targetRes->setValue(value);

    QModelIndex endpointIndex = index(endpointRow,0);
    QModelIndex changedIndex = index(resourceId, 0, endpointIndex);

    emit dataChanged(changedIndex, changedIndex, QVector<int>({ValueRole}));

    emit altered(RoomAlteration(m_endpoints.at(endpointRow),targetRes));
}


int RoomModel::findEndpointRowWithId(uint8_t id)
{
    for(uint8_t i = 0 ; i < m_endpoints.size() ; i++)
    {
        if(m_endpoints.at(i)->id() == id) return i;
    }
    return -1;
}

