#ifndef ENDPOINTITEM_H
#define ENDPOINTITEM_H

#include <QObject>
#include <QVector>

#include "resourceitem.h"


class EndpointItem : public QObject
{
    Q_OBJECT
public:
    explicit EndpointItem(uint8_t id, QString name, QString location, QVector<ResourceItem*> resources, QObject *parent = nullptr);

    QString name() const;
    QString location() const;
    uint8_t id() const;

    ResourceItem *getResource(int i);
    int countResources();

signals:

public slots:

private:
    uint8_t m_id;
    QString m_name;   
    QString m_location;
    QVector<ResourceItem*> m_resources;
};

#endif // ENDPOINTITEM_H
