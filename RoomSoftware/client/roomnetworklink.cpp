#include "roomnetworklink.h"
#include "roomlink.h"
#include "roomprotocol.h"

#define ABORT_INTERVAL  2000
#define RETRY_INTERVAL  2000
#define PING_INTERVAL   2000
#define PING_TIMEOUT    1000

RoomNetworkLink::RoomNetworkLink(QString hostName, quint16 port, QObject *parent) :
    QObject(parent),
    m_hostName(hostName), m_port(port),
    m_connected(false)
{
    m_socket = new QTcpSocket(this);
    connect(m_socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(onSocketError(QAbstractSocket::SocketError)));
    connect(m_socket, &QTcpSocket::connected,
            this, &RoomNetworkLink::onSocketConnected);
    connect(m_socket, &QTcpSocket::stateChanged,
            this, &RoomNetworkLink::onSocketStateChanged);

    m_link = new RoomLink(this);
    connect(m_link, &RoomLink::msgReceived,
            this, &RoomNetworkLink::onMsgFromLink);
    connect(m_link, &RoomLink::pingTimeout,
            this, &RoomNetworkLink::abort);

    m_abortTimer = new QTimer(this);
    m_abortTimer->setInterval(ABORT_INTERVAL);
    m_abortTimer->setSingleShot(true);
    connect(m_abortTimer, &QTimer::timeout,
            this, &RoomNetworkLink::abort);

    m_retryTimer = new QTimer(this);
    m_retryTimer->setInterval(RETRY_INTERVAL);
    m_retryTimer->setSingleShot(true);
    connect(m_retryTimer, &QTimer::timeout,
            this, &RoomNetworkLink::startConnection);

    m_link->setDevice(m_socket);
}

bool RoomNetworkLink::connected() const
{
    return m_connected;
}

QString RoomNetworkLink::hostname() const
{
    return m_hostName;
}

quint16 RoomNetworkLink::port() const
{
    return m_port;
}

void RoomNetworkLink::transmitMsg(RoomMessage msg)
{
    m_link->transmitMsg(msg);
}

void RoomNetworkLink::onSocketError(QAbstractSocket::SocketError)
{
    abort();
}

void RoomNetworkLink::onSocketConnected()
{
    m_abortTimer->stop();
    m_retryTimer->stop();
    m_link->autoPing(PING_INTERVAL, PING_TIMEOUT);
}

void RoomNetworkLink::onMsgFromLink(RoomMessage msg)
{
    msgReceived(msg);
}

void RoomNetworkLink::abort()
{
    m_retryTimer->start();
    m_abortTimer->stop();
    m_socket->disconnectFromHost();
}

void RoomNetworkLink::startConnection()
{
    m_retryTimer->stop();
    m_abortTimer->start();
    m_socket->connectToHost(m_hostName, m_port);
}

void RoomNetworkLink::onSocketStateChanged(QAbstractSocket::SocketState state)
{
    bool nowConnected = (state == QAbstractSocket::ConnectedState);
    if(nowConnected == m_connected) return;
    m_connected = nowConnected;
    emit connectedChanged(m_connected);
}
