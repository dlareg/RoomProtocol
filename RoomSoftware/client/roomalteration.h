#ifndef ROOMEVENT_H
#define ROOMEVENT_H

#include <QObject>
#include <QVariant>

class EndpointItem;
class ResourceItem;

class RoomAlteration
{
public:
    friend bool operator==(const RoomAlteration &a1, const RoomAlteration &a2);
    friend uint qHash(const RoomAlteration &a, uint seed);

    explicit RoomAlteration(QString endpoint, QString resource, QVariant value);
    explicit RoomAlteration(const EndpointItem *endpoint, const ResourceItem *resource);

    QString endpointName() const;
    QString resourceLabel() const;
    QVariant value() const;

    QString logString() const;

    bool isValid() const;

signals:

public slots:

private:
    QString m_endpointName;
    QString m_resourceLabel;
    QVariant m_value;
};

inline bool operator==(const RoomAlteration &a1, const RoomAlteration &a2)
{
    return (a1.m_endpointName == a2.m_endpointName) &&
            (a1.m_resourceLabel == a2.m_resourceLabel) &&
            (a1.m_value == a2.m_value);
}

inline uint qHash(const RoomAlteration &a, uint seed)
{
    return qHash(a.m_endpointName, seed) ^ qHash(a.m_resourceLabel, seed);
}


#endif // ROOMEVENT_H
