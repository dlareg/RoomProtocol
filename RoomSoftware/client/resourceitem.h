﻿#ifndef RESOURCEITEM_H
#define RESOURCEITEM_H

#include <QObject>
#include <QVariant>

class ResourceItem : public QObject
{
    Q_OBJECT
public:
    enum Type
    {
        Type_None       = 0x00,
        Type_Bool       = 0x01,
        Type_String     = 0x02,
        Type_Integer    = 0x03,
        Type_Color      = 0x04
    };
    Q_ENUM(Type)

    enum Capabilities
    {
        Cap_None        = 0x00,
        Cap_Notif       = 0x80,
        Cap_Exec        = 0x40,
        Cap_NotifExec   = Cap_Notif | Cap_Exec
    };
    Q_ENUM(Capabilities)


    explicit ResourceItem(uint8_t flags = 0x00, uint8_t maxLength = 0x00, QString label = QString());


    Type type() const ;
    Capabilities caps() const;
    QString label() const;
    uint8_t maxLength() const;

    QVariant value() const;
    void setValue(QVariant value);

private:
    uint8_t m_flags;
    uint8_t m_maxLength;
    QString m_label;
    QVariant m_value;
};

#endif // RESOURCEITEM_H
