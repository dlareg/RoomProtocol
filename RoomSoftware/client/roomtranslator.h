#ifndef ROOMTRANSLATOR_H
#define ROOMTRANSLATOR_H

#include <QObject>
#include "roommessage.h"
#include "resourceitem.h"

class RoomModel;
class EndpointItem;

class RoomTranslator : public QObject
{
    Q_OBJECT
public:
    explicit RoomTranslator(RoomModel *model, QObject *parent = nullptr);
    Q_INVOKABLE void requestDiscovery();

public slots:
    void msgFromEndpoint(RoomMessage msg);

signals:
    void msgToEndpoint(RoomMessage msg);

protected:
    EndpointItem *createEndpointFromDescriptorFrame(int endpointId, RoomMessage frame);

    QVariant argsToValue(ResourceItem::Type type, QByteArray args);
    QVariant boolArgsToValue(QByteArray args);
    QVariant stringArgsToValue(QByteArray args);
    QVariant integerArgsToValue(QByteArray args);
    QVariant colorArgsToValue(QByteArray args);

    void resetEndpoint(EndpointItem* endpoint);
    bool sendCommand(EndpointItem* endpoint, int resourceIndex, QVariant value);
    QByteArray boolValueToArgs(QVariant value);
    QByteArray stringValueToArgs(QVariant value);
    QByteArray colorValueToArgs(QVariant value);
    QByteArray integerValueToArgs(QVariant value);

private:
    RoomModel *m_model;
    QStringList m_endpointsUris;
};

#endif // ROOMTRANSLATOR_H
