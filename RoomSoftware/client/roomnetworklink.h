#ifndef ROOMNETWORKLINK_H
#define ROOMNETWORKLINK_H

#include <QObject>
#include <QTcpSocket>
#include <QTimer>
#include <QUrl>
#include <QMap>

#include "roommessage.h"

class RoomLink;

class RoomNetworkLink : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)

public:

    explicit RoomNetworkLink(QString hostName, quint16 port, QObject *parent = nullptr);

    bool connected() const;
    QString hostname() const;
    quint16 port() const;

    void startConnection();

public slots:
    void transmitMsg(RoomMessage msg);

signals:
    void msgReceived(RoomMessage msg);
    void connectedChanged(bool connected);

private slots:
    void abort();
    void onSocketError(QAbstractSocket::SocketError);
    void onSocketConnected();
    void onMsgFromLink(RoomMessage msg);

private:
    QTcpSocket *m_socket;
    RoomLink *m_link;
    QTimer* m_abortTimer;
    QTimer* m_retryTimer;
    QString m_hostName;
    quint16 m_port;
    bool m_connected;

    void onSocketStateChanged(QAbstractSocket::SocketState state);
};

#endif // ROOMNETWORKLINK_H
