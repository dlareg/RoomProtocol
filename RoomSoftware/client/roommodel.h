#ifndef ROOMMODEL_H
#define ROOMMODEL_H

#include "roommessage.h"
#include "resourceitem.h"
#include "roomalteration.h"

#include <QPointer>
#include <QAbstractListModel>

class EndpointItem;

class RoomModel : public QAbstractItemModel
{
    Q_OBJECT

public:
    enum ItemRoles
    {  
        ValueRole = Qt::UserRole + 1,
        TypeRole,
        CapsRole,
        LengthRole,
        isEndpointRole,
        LocationRole,
        ResetRole
    };

    explicit RoomModel(QObject *parent = nullptr);

    virtual QHash<int, QByteArray> roleNames() const override;
    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
    virtual QModelIndex parent(const QModelIndex &child) const override;
    virtual int columnCount(const QModelIndex &parent) const override;
    virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    void clearEndpoints();
    void insertEndpoint(uint8_t endpointId, EndpointItem* endpoint);

    const ResourceItem *getEndpointResource(uint8_t endpointId, uint8_t resourceId);
    void setEndpointResourceValue(uint8_t endpointId, uint8_t resourceId, QVariant value);

signals:
    void commandToSend(EndpointItem* endpoint, int resourceId, QVariant value);
    void requestEndpointReset(EndpointItem* endpoint);

    void altered(RoomAlteration alteration);

protected:
    int findEndpointRowWithId(uint8_t id);

private:

    QVector<EndpointItem*> m_endpoints;
};

#endif // ROOMMODEL_H
