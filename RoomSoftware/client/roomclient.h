#ifndef ROOMCLIENT_H
#define ROOMCLIENT_H

#include <QObject>

#include "roommodel.h"
#include "resourceitem.h"
#include "roomnetworklink.h"
#include "roomalteration.h"

class RoomTranslator;
class Multiplexer;

class RoomClient : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)
public:
    explicit RoomClient(QObject *parent = nullptr);

    void connectToNetworkHost(QString hostName, quint16 port);
    void connectToLocalEndpoints();

    bool connected() const;

    RoomModel *getModel();

signals:
    void connectedChanged(bool connected);
    void roomAltered(RoomAlteration alteration);
    void logOut(QString logString);

public slots:

private slots:
    void onNetworkConnectedChanged(bool connected);
    void setConnected(bool connected);

protected:
    void logMessage(QString msg);

private:
    RoomModel* m_model;
    RoomTranslator* m_translator;
    RoomNetworkLink* m_netLink;
    Multiplexer* m_multiplexer;

    bool m_connected;
};

#endif // ROOMCLIENT_H
