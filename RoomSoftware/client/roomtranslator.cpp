#include "roomtranslator.h"

#include "roommessage.h"
#include "roomprotocol.h"

#include "roommodel.h"
#include "endpointitem.h"

#include <QVariant>
#include <QColor>
#include <QDebug>

RoomTranslator::RoomTranslator(RoomModel *model, QObject *parent) :
    QObject(parent),
    m_model(model)
{
    connect(m_model, &RoomModel::commandToSend,
            this, &RoomTranslator::sendCommand);
    connect(m_model, &RoomModel::requestEndpointReset,
            this, &RoomTranslator::resetEndpoint);
}

void RoomTranslator::requestDiscovery()
{
    qDebug() << "Request discovery";
    QByteArray args;
    args.append(char(ROOM_NODE_DISCOVR_CACHE));
    RoomMessage msg(ROOM_VERB_WRITE_EXEC, ROOM_NODE_CMD_DISCOVERY, args);
    msg.pushAddress(ROOM_NODE_ENDPOINT_ID);
    emit msgToEndpoint(msg);
}

void RoomTranslator::resetEndpoint(EndpointItem* endpoint)
{
    uint8_t id = endpoint->id();
    qDebug() << "Requested reset of endpoint" << id;
    QByteArray args;
    args.append(char(id));
    RoomMessage msg(ROOM_VERB_WRITE_EXEC, ROOM_NODE_CMD_RESET, args);
    msg.pushAddress(ROOM_NODE_ENDPOINT_ID);
    emit msgToEndpoint(msg);
}

void RoomTranslator::msgFromEndpoint(RoomMessage msg)
{
    if(!msg.isComplete() || msg.addressCount() != 1) return;

    int endpointId = msg.popAddress();

    if(endpointId == ROOM_NODE_ENDPOINT_ID)
    {
        if( msg.noun() == ROOM_NODE_CMD_DISCOVERY &&
                msg.verb() == ROOM_VERB_ACK)
        {
            m_endpointsUris = QString::fromLatin1(msg.args()).split(QChar(ROOM_BYTE_SEP));
            m_model->clearEndpoints();
            qDebug() << "Cleared endpoints";
        }
    }

    else if(msg.noun() == ROOM_DESCRIPTOR_ID)
    {
        if((msg.verb() == ROOM_VERB_ACK ||
            msg.verb() == ROOM_VERB_RESET_DONE))
        {
            EndpointItem* endpoint = createEndpointFromDescriptorFrame(endpointId, msg);
            qDebug() << "Created endpoint" << endpoint->name() << endpoint->location();
            m_model->insertEndpoint(endpointId, endpoint);
        }
    }

    else if(msg.verb() == ROOM_VERB_ACK ||
            msg.verb() == ROOM_VERB_VALUE_CHANGED ||
            msg.verb() == ROOM_VERB_DESC_REQ ||
            msg.verb() == ROOM_VERB_RESET_DONE )
    {
        const int resourceIndex = msg.noun() - 1;
        const ResourceItem* targetResource = m_model->getEndpointResource(endpointId, resourceIndex);
        if(targetResource)
        {
            QVariant value = argsToValue(targetResource->type(), msg.args());
            m_model->setEndpointResourceValue(endpointId, resourceIndex, value);
        }
    }
}


EndpointItem *RoomTranslator::createEndpointFromDescriptorFrame(int endpointId, RoomMessage frame)
{
    QList<QByteArray> fields = frame.args().split(ROOM_BYTE_SEP);

    QString name = QString("Malformed endpoint");
    QVector<ResourceItem*> resources;

    //Check length is valid and correct magic string (protocol name)
    if(fields.size() >= ROOM_DESC_MIN_VALID_FIELDS &&
            fields.at(ROOM_DESC_INDEX_MAGICSTRING) == ROOM_MAGIC_STRING)
    {
        name = fields.at(ROOM_DESC_INDEX_EPNAME);

        for(QList<QByteArray>::const_iterator i = fields.constBegin() + ROOM_DESC_MIN_VALID_FIELDS ;
            i != fields.constEnd() ; i++)
        {
            uint8_t resFlag         = i->at(ROOM_DESC_RES_INDEX_FLAG);
            uint8_t resMaxLength    = i->at(ROOM_DESC_RES_INDEX_LENGTH);
            QString resLabel        = i->right(i->length() - ROOM_DESC_RES_INDEX_LABEL);
            resources.append(new ResourceItem(resFlag, resMaxLength, resLabel));
        }

    }

    QString location =(endpointId <= m_endpointsUris.size())
            ? m_endpointsUris.at(endpointId-1)
            : "";

    return new EndpointItem(endpointId, name, location, resources);
}

#warning USES RESOURCE TO DETERMINE TYPE AND LENGTH, THEN IMPLEMENT INTEGER PROPERLY

QVariant RoomTranslator::argsToValue(ResourceItem::Type type, QByteArray args)
{
#warning RETHINK PARSER : MUST USES RESOURCE SIZE ! ( USE STRUCT WITH POSSIBLE TYPE / SIZE COMBINATION AND PARSER TO USE ?)
    switch(type)
    {
    case ResourceItem::Type_Bool:
        return boolArgsToValue(args);

    case ResourceItem::Type_String:
        return stringArgsToValue(args);

    case ResourceItem::Type_Integer:
        return integerArgsToValue(args);

    case ResourceItem::Type_Color:
        return colorArgsToValue(args);

    default:
        return QVariant();
    }
}

QVariant RoomTranslator::boolArgsToValue(QByteArray args)
{
    return (args.length() == 1) ? QVariant::fromValue(args.at(0) != 0x00) : QVariant();
}

QVariant RoomTranslator::stringArgsToValue(QByteArray args)
{
    return QVariant::fromValue(QString::fromLocal8Bit(args));
}

QVariant RoomTranslator::integerArgsToValue(QByteArray args)
{
    qDebug() << "INTEGER PARSING NOT IMPLEMENTED";
    return QVariant();
}

QVariant RoomTranslator::colorArgsToValue(QByteArray args)
{
    QColor color;
    if(args.length() == 3)
    {
        color.setRed(uint8_t(args.at(0)));
        color.setGreen(uint8_t(args.at(1)));
        color.setBlue(uint8_t(args.at(2)));
        color.setAlpha(255);
    }
    return color;
}

bool RoomTranslator::sendCommand(EndpointItem *endpoint, int resourceIndex, QVariant value)
{
    ResourceItem *res = endpoint->getResource(resourceIndex);

    if(!res) return false;

    QByteArray args;

    switch(res->type())
    {
    case ResourceItem::Type_Bool:
        args = boolValueToArgs(value);
        break;

    case ResourceItem::Type_Color:
        args = colorValueToArgs(value);
        break;

    case ResourceItem::Type_Integer:
        args = integerValueToArgs(value);
        break;

    case ResourceItem::Type_String:
        args = stringValueToArgs(value);
        break;

    default:
        return false;
    }

    RoomMessage msg(ROOM_VERB_WRITE_EXEC, resourceIndex+1, args);
    msg.pushAddress(endpoint->id());
    emit msgToEndpoint(msg);
    return true;
}

QByteArray RoomTranslator::boolValueToArgs(QVariant value)
{
    QByteArray args;
    args.append( value.toBool() ? 0x01 : 0x00);
    return args;
}

QByteArray RoomTranslator::stringValueToArgs(QVariant value)
{
    return value.toString().toLatin1();
}

QByteArray RoomTranslator::colorValueToArgs(QVariant value)
{
    QByteArray a;
    QColor c = value.value<QColor>();
    a.append(c.red());
    a.append(c.green());
    a.append(c.blue());
    return a;
}

QByteArray RoomTranslator::integerValueToArgs(QVariant value)
{
    qDebug() << "INTEGER TRANSMISSION NOT IMPLEMENTED";
    return QByteArray();
}
