#include "roomalteration.h"

#include "endpointitem.h"
#include "resourceitem.h"

RoomAlteration::RoomAlteration(QString endpoint, QString resource, QVariant value) :
    m_endpointName(endpoint),
    m_resourceLabel(resource),
    m_value(value)
{

}

RoomAlteration::RoomAlteration(const EndpointItem *endpoint, const ResourceItem *resource) :
    m_endpointName(endpoint->name()),
    m_resourceLabel(resource->label()),
    m_value(resource->value())
{
}

QString RoomAlteration::endpointName() const
{
    return m_endpointName;
}

QString RoomAlteration::resourceLabel() const
{
    return m_resourceLabel;
}

QVariant RoomAlteration::value() const
{
    return m_value;
}

QString RoomAlteration::logString() const
{
    return QStringLiteral("%1/%2 changed to %3")
            .arg(m_endpointName)
            .arg(m_resourceLabel)
            .arg(m_value.toString());
}

bool RoomAlteration::isValid() const
{
    return !m_endpointName.isEmpty() && !m_resourceLabel.isEmpty() && m_value.isValid();
}

