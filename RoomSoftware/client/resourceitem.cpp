#include "resourceitem.h"
#include "roomprotocol.h"

ResourceItem::ResourceItem(uint8_t flags, uint8_t maxLength, QString label) :
    m_flags(flags), m_maxLength(maxLength), m_label(label)
{

}

ResourceItem::Type ResourceItem::type() const
{
    return Type(m_flags & ROOM_DESC_BITS_TYPE);
}

ResourceItem::Capabilities ResourceItem::caps() const
{
    return Capabilities(m_flags & ROOM_DESC_BITS_CAPS);
}

QString ResourceItem::label() const
{
    return m_label;
}

uint8_t ResourceItem::maxLength() const
{
    return m_maxLength;
}

QVariant ResourceItem::value() const
{
    return m_value;
}

void ResourceItem::setValue(QVariant value)
{
    m_value = value;
}
