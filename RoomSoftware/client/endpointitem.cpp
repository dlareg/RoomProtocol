#include "endpointitem.h"


EndpointItem::EndpointItem(uint8_t id, QString name, QString location, QVector<ResourceItem*> resources, QObject *parent) : QObject(parent),
    m_id(id), m_name(name), m_location(location), m_resources(resources)
{
    foreach (ResourceItem *res, m_resources) res->setParent(this);
}

QString EndpointItem::name() const
{
    return m_name;
}

QString EndpointItem::location() const
{
    return m_location;
}

uint8_t EndpointItem::id() const
{
    return m_id;
}

ResourceItem *EndpointItem::getResource(int i)
{
    return (i < m_resources.size()) ? m_resources[i] : nullptr;
}

int EndpointItem::countResources()
{
    return m_resources.size();
}
