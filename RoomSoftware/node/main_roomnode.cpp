#include <QCoreApplication>

#include <QTcpServer>
#include <QTcpSocket>

#include "multiplexer.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    //Node related
    Multiplexer mux;
    QTcpServer *serv = new QTcpServer();

    QObject::connect(serv, &QTcpServer::newConnection, [&] () {
        QTcpSocket* socket = serv->nextPendingConnection();

        qDebug() << "New connection from" << socket->peerAddress();

        RoomLink* link = new RoomLink();
        link->setDevice(socket);
        socket->setParent(link);

        QObject::connect(link, &RoomLink::msgReceived, &mux, &Multiplexer::msgToEndpoint);
        QObject::connect(&mux, &Multiplexer::msgFromEndpoint, link, &RoomLink::transmitMsg);
        QObject::connect(socket, &QTcpSocket::disconnected, link, &RoomLink::deleteLater);
        QObject::connect(socket, &QTcpSocket::disconnected, [&] () {
            qDebug() << "Connection closed";
        });

    });
    if(serv->listen( QHostAddress::Any, 15685))
        qDebug() << "Listening on port" << serv->serverPort();
    else
        qDebug() << "Failed to listen on port" << serv->serverPort();

    QMetaObject::invokeMethod(&mux, "doEndpointsDiscovery", Qt::QueuedConnection);

    return a.exec();
}
