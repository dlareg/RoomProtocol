#ifndef SERIALWORKER_H
#define SERIALWORKER_H

#include "roomlink.h"
#include "roommessage.h"
#include <QObject>

class QSerialPort;

class SerialWorker : public QObject
{
    Q_OBJECT
public:
    SerialWorker(QString portName);

    Q_INVOKABLE void open();
    Q_INVOKABLE void close();
    Q_INVOKABLE void reset();
    Q_INVOKABLE void transmitFrame(RoomMessage msg);

signals:
    void frameReceived(RoomMessage frame);

private:
    QString m_portName;
    QSerialPort* m_serialPort;
    RoomLink m_link;
};


#endif // SERIALWORKER_H
