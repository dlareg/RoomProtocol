#ifndef ABSTRACTENDPOINT_H
#define ABSTRACTENDPOINT_H

#include "roommessage.h"
#include <QObject>

class AbstractEndpoint : public QObject
{
    Q_OBJECT
public:
    explicit AbstractEndpoint(QObject *parent = nullptr);
    bool operator==(const AbstractEndpoint& other);

    virtual QString getUri() const = 0;

signals:
    void frameReceived(RoomMessage frame);

public slots:
    virtual void open() = 0;
    virtual void close() = 0;
    virtual void reset() = 0;
    virtual void transmitFrame(RoomMessage frame) = 0;

protected:


private:

};

#endif // ABSTRACTENDPOINT_H
