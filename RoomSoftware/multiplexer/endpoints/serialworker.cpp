#include "serialworker.h"
#include "roomprotocol.h"

#include <QThread>
#include <QSerialPort>



#define RESET_DURATION_MS   10


SerialWorker::SerialWorker(QString portName) :
    m_portName(portName),
    m_serialPort(nullptr)
{
    connect(&m_link,&RoomLink::msgReceived,
            this, &SerialWorker::frameReceived);
}

void SerialWorker::open()
{
    close();
    m_serialPort = new QSerialPort(m_portName,this);
    m_serialPort->setBaudRate(115200);
    m_serialPort->setParity(QSerialPort::NoParity);
    m_serialPort->setFlowControl(QSerialPort::NoFlowControl);
    m_serialPort->setStopBits(QSerialPort::OneStop);
    if(m_serialPort->open(QIODevice::ReadWrite))
        m_link.setDevice(m_serialPort);
    else
        close();
}

void SerialWorker::close()
{
    m_link.setDevice(nullptr);
    if(m_serialPort)
    {
        m_serialPort->close();
        m_serialPort = nullptr;
    }
}

void SerialWorker::reset()
{
    if(m_serialPort)
    {
        m_serialPort->setDataTerminalReady(false);
        thread()->msleep(RESET_DURATION_MS);
        m_serialPort->setDataTerminalReady(true);
    }
}

void SerialWorker::transmitFrame(RoomMessage msg)
{
    m_link.transmitMsg(msg);
}
