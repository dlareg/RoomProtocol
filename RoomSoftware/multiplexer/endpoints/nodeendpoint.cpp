#include "nodeendpoint.h"

#include "roomprotocol.h"
#include "multiplexer.h"

#include <QStringList>

NodeEndpoint::NodeEndpoint(Multiplexer *parent) :
    AbstractEndpoint(parent),
    m_mux(parent)
{

}

void NodeEndpoint::open()
{
    return;
}

void NodeEndpoint::close()
{
    return;
}

void NodeEndpoint::reset()
{
    return;
}

void NodeEndpoint::transmitFrame(RoomMessage frame)
{
    QByteArray args = frame.args();
    uint8_t result = ROOM_VERB_ERR_UNKNOWN_NOUN;

    switch(frame.noun())
    {
    case ROOM_NODE_CMD_DISCOVERY:
        result = processDiscoveryRequest(&args) ? ROOM_VERB_ACK : ROOM_VERB_NACK;
        break;
    case ROOM_NODE_CMD_RESET:
        result = processResetRequest(&args) ? ROOM_VERB_ACK : ROOM_VERB_NACK;
        break;
    case TOOM_NODE_CMD_LOCATE:
        result = processLocateReqest(&args) ? ROOM_VERB_ACK : ROOM_VERB_NACK;
    default:
        args.clear();
        break;
    }

    emit AbstractEndpoint::frameReceived(RoomMessage(result, frame.noun(), args));
}

bool NodeEndpoint::processDiscoveryRequest(QByteArray *args)
{
    QByteArray cmdArgs;
    cmdArgs.swap(*args);

    if(cmdArgs.length() != 1) return false;

    QStringList endpointsUriList;

    //Execute discovery
    switch(cmdArgs.at(0))
    {
    case ROOM_NODE_DISCOVR_CACHE:
        endpointsUriList = m_mux->doAllEndpointDescriptorRequest();
        break;

    case ROOM_NODE_DISCOVR_HARD:
        endpointsUriList = m_mux->doEndpointsDiscovery();
        break;

#warning ADD ROOM_NODE_DISCOVR_SOFT

    default:
        return false;
    }

    //Return endpoints locations
    (*args) = endpointsUriList.join(QChar(ROOM_BYTE_SEP)).toLatin1();
    return true;
}

bool NodeEndpoint::processResetRequest(QByteArray *args)
{
    QByteArray cmdArgs;
    cmdArgs.swap(*args);

    if(cmdArgs.length() != 1) return false;

    return m_mux->doEndpointReset(cmdArgs.at(0));
}

bool NodeEndpoint::processLocateReqest(QByteArray *args)
{
    QByteArray cmdArgs;
    cmdArgs.swap(*args);

    if(cmdArgs.length() != 1) return false;

    (*args) = m_mux->getUriForEndpoint(cmdArgs.at(0)).toLatin1();

    return true;
}

QString NodeEndpoint::getUri() const
{
    return QStringLiteral("NODE");
}
