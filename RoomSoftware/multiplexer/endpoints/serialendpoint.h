#ifndef SERIALENDPOINT_H
#define SERIALENDPOINT_H

#include "abstractendpoint.h"
#include <QThread>
#include <QPointer>
#include <QVector>

class SerialWorker;

class SerialEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:
    SerialEndpoint(QString portName, QObject *parent = 0);
    ~SerialEndpoint();

    static QVector<AbstractEndpoint *> discoverEndpoints();

public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

protected:
    QString getUri() const override;

private:
    const QString m_serialPortName;
    QPointer<SerialWorker> m_worker;
    QPointer<QThread> m_thread;

};

#endif // SERIALENDPOINT_H
