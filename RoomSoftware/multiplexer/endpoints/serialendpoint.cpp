#include "serialendpoint.h"
#include "serialworker.h"
#include <QSerialPortInfo>
#include <QDebug>

QVector<AbstractEndpoint*> SerialEndpoint::discoverEndpoints()
{
    QList<QSerialPortInfo> portList = QSerialPortInfo::availablePorts();
    QVector<AbstractEndpoint*> endpoints;

    for(QList<QSerialPortInfo>::const_iterator it = portList.constBegin() ;
        it != portList.constEnd();
        it++)
    {
        endpoints.append(new SerialEndpoint(it->portName()));
    }

    return endpoints;
}

SerialEndpoint::SerialEndpoint(QString portName, QObject *parent) :
    AbstractEndpoint(parent),
    m_serialPortName(portName),
    m_worker(nullptr),
    m_thread(nullptr)
{

}

SerialEndpoint::~SerialEndpoint()
{
    if(m_thread.isNull()) return;
    m_thread->quit();
    if(!m_thread->wait(3000)) //Wait until it actually has terminated (max. 3 sec)
    {
        m_thread->terminate(); //Thread didn't exit in time, probably deadlocked, terminate it!
        m_thread->wait(); //We have to wait again here!
    }
}

void SerialEndpoint::open()
{
    close();
    m_worker = new SerialWorker(m_serialPortName);
    m_thread = new QThread(this);
    m_worker->moveToThread(m_thread);
    connect(m_worker, &SerialWorker::frameReceived, this, &SerialEndpoint::frameReceived,Qt::QueuedConnection);
    connect(m_thread, &QThread::started, m_worker, &SerialWorker::open );
    connect(m_thread, &QThread::finished, m_worker, &SerialWorker::close );
    connect(m_thread, &QThread::finished, m_thread, &QThread::deleteLater );
    connect(m_thread, &QThread::finished, m_worker, &SerialWorker::deleteLater );
    m_thread->start();
}

void SerialEndpoint::close()
{
    if(m_thread.isNull()) return;
    m_thread->quit();
}

void SerialEndpoint::reset()
{
    if(m_thread.isNull()) return;
    QMetaObject::invokeMethod(m_worker, "reset", Qt::QueuedConnection);
}

void SerialEndpoint::transmitFrame(RoomMessage frame)
{
    if(m_thread.isNull()) return;
    if(!QMetaObject::invokeMethod(m_worker, "transmitFrame", Qt::QueuedConnection, Q_ARG(RoomMessage, frame)))
    {
        qDebug() << "Failed to invoke transmit method on serial thread";
    }
}

QString SerialEndpoint::getUri() const
{
    return QStringLiteral("SER:")+m_serialPortName;
}
