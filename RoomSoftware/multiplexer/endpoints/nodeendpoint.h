#ifndef NODEENDPOINT_H
#define NODEENDPOINT_H

#include "abstractendpoint.h"
#include "roomlink.h"

#include <QObject>
#include <QMap>

class Multiplexer;

class NodeEndpoint : public AbstractEndpoint
{
    Q_OBJECT
public:
    explicit NodeEndpoint(Multiplexer *parent = nullptr);

public slots:

    // AbstractEndpoint interface
public slots:
    void open() override;
    void close() override;
    void reset() override;
    void transmitFrame(RoomMessage frame) override;

protected:

    QString getUri() const override;

    bool processDiscoveryRequest(QByteArray *args);
    bool processResetRequest(QByteArray *args);
    bool processLocateReqest(QByteArray *args);

    Multiplexer* m_mux;
};

#endif // NODEENDPOINT_H
