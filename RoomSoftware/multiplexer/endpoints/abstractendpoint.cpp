#include "abstractendpoint.h"

#include <QThread>

AbstractEndpoint::AbstractEndpoint(QObject *parent) :
    QObject(parent)
{

}

bool AbstractEndpoint::operator==(const AbstractEndpoint &other)
{
    return this->getUri() == other.getUri();
}
