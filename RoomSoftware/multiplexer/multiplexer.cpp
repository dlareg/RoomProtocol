#include "multiplexer.h"
#include "endpoints/serialendpoint.h"
#include "roomprotocol.h"
#include <QDebug>

Multiplexer::Multiplexer(QObject *parent) : QObject(parent)
{
    //Endpoint 0 is node control endpoint
    m_nodeEndpoint = new NodeEndpoint(this);

    connect(this, &Multiplexer::destroyed, []() {
        qDebug() << "Multiplexer : WARNING multiplexer destroyed";
    });
    connect(m_nodeEndpoint, &NodeEndpoint::destroyed, []() {
        qDebug() << "Multiplexer : WARNING node endpoint destroyed";
    });
    connect(m_nodeEndpoint, &NodeEndpoint::frameReceived,
            this, &Multiplexer::onMsgReceived);
}

QStringList Multiplexer::doEndpointsDiscovery()
{ 
    //TODO :  delete all endpoints managed but not available anymore
    //Currently we simply delete ALL endpoints
    while(!m_endpoints.isEmpty())
    {
        AbstractEndpoint* currEndpoint = m_endpoints.takeLast();
        currEndpoint->close();
        currEndpoint->deleteLater();
    }

    //Query all possible endpoints types
    m_endpoints.append(SerialEndpoint::discoverEndpoints());

    //Then add available endpoints not managed yet
    for(int i = 0 ; i < m_endpoints.size() ; i++)
    {
        AbstractEndpoint* currEndpoint = m_endpoints.at(i);
        currEndpoint->setParent(this);
        connect(currEndpoint, &AbstractEndpoint::frameReceived, this, &Multiplexer::onMsgReceived);
        currEndpoint->open();
    }

    qDebug() << "Discovered" << m_endpoints.size() << "endpoints";

    return buildEndpointsUriList();
}

QStringList Multiplexer::doAllEndpointDescriptorRequest()
{
    foreach(AbstractEndpoint* endpoint, m_endpoints)
    {
        endpoint->transmitFrame(RoomMessage(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID));
    }

    return buildEndpointsUriList();
}

bool Multiplexer::doEndpointReset(int endpointId)
{
    AbstractEndpoint* endpoint = getEndpointFromId(endpointId);
    if(!endpoint) return false;
    endpoint->reset();
    return true;
}

bool Multiplexer::doEndpointDescriptorRequest(int endpointId)
{
    AbstractEndpoint* endpoint = getEndpointFromId(endpointId);
    if(!endpoint) return false;
    endpoint->transmitFrame(RoomMessage(ROOM_VERB_READ, ROOM_DESCRIPTOR_ID));
    return true;
}

QString Multiplexer::getUriForEndpoint(int endpointId)
{
    AbstractEndpoint* endpoint = getEndpointFromId(endpointId);
    return endpoint ? endpoint->getUri() : QString();
}

void Multiplexer::msgToEndpoint(RoomMessage msg)
{
    if(msg.addressCount() != 1) return;
    AbstractEndpoint* endpoint = getEndpointFromId(msg.popAddress());
    if(endpoint) endpoint->transmitFrame(msg);
}

void Multiplexer::onMsgReceived(RoomMessage msg)
{
    AbstractEndpoint* senderEndpoint = qobject_cast<AbstractEndpoint*>(sender());

    if(senderEndpoint == m_nodeEndpoint)
    {
        msg.pushAddress(ROOM_NODE_ENDPOINT_ID);
        emit msgFromEndpoint(msg);
    }
    else
    {       
        int senderId = m_endpoints.indexOf(senderEndpoint);

        if(senderId >= 0)
        {
            msg.pushAddress(senderId+1);
            emit msgFromEndpoint(msg);
        }
    }
}

AbstractEndpoint *Multiplexer::getEndpointFromId(int id)
{
    if(id == ROOM_NODE_ENDPOINT_ID) return m_nodeEndpoint;
    else if(id <= m_endpoints.size()) return m_endpoints.at(id-1);
    else return nullptr;
}

QStringList Multiplexer::buildEndpointsUriList()
{
    QStringList r;

    for(auto it = m_endpoints.constBegin();
        it < m_endpoints.constEnd();
        it++)
    {
        r.append((*it)->getUri());
    }

    return r;
}

