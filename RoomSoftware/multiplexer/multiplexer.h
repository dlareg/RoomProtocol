#ifndef MULTIPLEXER_H
#define MULTIPLEXER_H

#include "roommessage.h"
#include "endpoints/nodeendpoint.h"
#include <QObject>
#include <QVector>

class AbstractEndpoint;

class Multiplexer : public QObject
{
    Q_OBJECT
public:
    explicit Multiplexer(QObject *parent = nullptr);

public slots:
    QStringList doEndpointsDiscovery();
    QStringList doAllEndpointDescriptorRequest();
    bool doEndpointReset(int endpointId);
    bool doEndpointDescriptorRequest(int endpointId);
    QString getUriForEndpoint(int endpointId);

// ACTUAL INTERFACE TO CONTROLPANEL
public slots:
    void msgToEndpoint(RoomMessage msg);
signals:
    void msgFromEndpoint(RoomMessage msg);

protected:
    void onMsgReceived(RoomMessage msg);
    AbstractEndpoint *getEndpointFromId(int id);
    QStringList buildEndpointsUriList();

private:
    NodeEndpoint* m_nodeEndpoint;
    QVector<AbstractEndpoint*> m_endpoints;
};

#endif // MULTIPLEXER_H
