#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QString>
#include <QtQml>

#include <QHostAddress>

#include <QCommandLineParser>

#include "roomclient.h"


int main(int argc, char *argv[])
{
    QGuiApplication a(argc, argv);

    QCommandLineParser parser;
    parser.addPositionalArgument("Host IP"  , "Host address");
    parser.addPositionalArgument("Host port", "Host port");
    parser.process(a);

    RoomClient client;

    qmlRegisterUncreatableType<ResourceItem>
            ("RoomResource"     , 1 , 0,    "RoomResource",     "This type in only usable to access enums");

    //Create QML application
    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();
    ctxt->setContextProperty("roomclient", &client);
    ctxt->setContextProperty("endpoints", qobject_cast<QAbstractItemModel*>(client.getModel()));

    engine.load(QUrl("qrc:/qml/main.qml"));

    //Failed to load QML ? EXIT
    if(engine.rootObjects().isEmpty()) return -1;

    QStringList arguments = parser.positionalArguments();

    if(arguments.isEmpty())
    {
        client.connectToLocalEndpoints();
    }
    else
    {
        QString hostAddress = arguments.at(0);
        int port = 0;
        if(arguments.size() > 1) arguments.at(1).toInt();
        client.connectToNetworkHost(hostAddress, port);
    }

    return a.exec();
}
