import QtQuick 2.0
import QtQuick.Controls 1.2

TextField {

    function setup(model)
    {
        value = model.value
        text = value
        focus = true
        maximumLength = model.length
    }

    onTextChanged: value = text
}
