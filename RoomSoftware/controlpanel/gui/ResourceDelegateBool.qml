import QtQuick 2.0
import QtQuick.Controls 1.4


Text {
    font.bold: true
    text: value ? "ON" : "OFF"
    color: value ? "green" : "red";
}
