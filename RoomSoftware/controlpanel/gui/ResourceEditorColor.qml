import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.2

ColumnLayout {
    function setup(model) {
        value = model.value
        rVal.value = model.value.r * 255
        gVal.value = model.value.g * 255
        bVal.value = model.value.b * 255
        delegateLoader.setSource("ResourceDelegateColor.qml")
    }

    function computeColor()
    {
        value = Qt.rgba(rVal.value / 255, gVal.value / 255, bVal.value / 255, 1)
    }

    Loader {
        id: delegateLoader
        Layout.alignment: Qt.AlignHCenter
    }

    Row {
        Layout.alignment: Qt.AlignHCenter

        SpinBox {
            id:rVal
            prefix: "R: "
            minimumValue: 0
            maximumValue: 255
            stepSize: 1
            onValueChanged: computeColor()
        }
        SpinBox {
            id: gVal
            prefix: "G: "
            minimumValue: 0
            maximumValue: 255
            stepSize: 1
            onValueChanged: computeColor()
        }
        SpinBox {
            id: bVal
            prefix: "B: "
            minimumValue: 0
            maximumValue: 255
            stepSize: 1
            onValueChanged: computeColor()
        }
    }
}
