import QtQuick 2.0

ResourceDelegateBool {
    function setup(model) {
        value = !model.value
    }
}
