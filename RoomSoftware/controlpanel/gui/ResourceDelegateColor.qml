import QtQuick 2.0


Row {
    spacing: 5
    Rectangle {
        id: indicator
        height: parent.height
        width: height
        radius: height / 2
        border.width: 2
        border.color: "black"
        color: value ? value : "transparent"
    }
    Text {
        id: name
        text: value ? value.toString().toUpperCase() : ""
        font.bold: true
    }
}

