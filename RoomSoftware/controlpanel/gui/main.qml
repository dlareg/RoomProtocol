import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Window 2.2
import QtQuick.Layouts 1.0
import QtQml.Models 2.2
import RoomResource 1.0

Window {
    id: mainWindow
    objectName: "mainWindow"
    minimumWidth: 240
    minimumHeight: 320
    width: 320
    height: 480
    visible: true

    SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical

        TreeView {
            id: treeview

            Layout.minimumHeight:200
            Layout.fillHeight: true

            headerVisible: true
            enabled: roomclient.connected

            model: endpoints

            selection: ItemSelectionModel {
                model: endpoints
            }

            TableViewColumn {
                id: labelColumn
                title: "Label"
                role: "label"
                width: 150
                delegate: Component {
                    Text{
                        text: styleData.value
                        clip:true
                        MouseArea{
                            anchors.fill: parent
                            onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                            onDoubleClicked: treeview.openDialog(model, styleData.index)
                        }
                    }
                }
            }

            TableViewColumn {
                id: capsColumn
                title: "Caps"
                role: "caps"
                width: 25
                delegate: Component {
                    MouseArea{
                        onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                        onDoubleClicked: treeview.openDialog(model, styleData.index)
                        Image{
                            source: "qrc:/icons/edit"
                            visible:switch(styleData.value * 1) {
                                    case RoomResource.Cap_Exec:
                                    case RoomResource.Cap_NotifExec:
                                        return true;
                                    default:
                                        return false;
                                    }
                            fillMode: Image.PreserveAspectFit
                            scale: 0.7
                        }
                    }
                }
            }

            TableViewColumn {
                id: valueColumn
                title: "Value"
                role: "value"
                width: 200
                delegate: Component {
                    Rectangle {
                        id: rect
                        color: "transparent"
                        ColorAnimation{
                            id: flashAnimation
                            from: "#FFFFFF00"
                            to: "#00FFFF00"
                            duration: 2000
                            target: rect
                            property: "color"
                            easing.type: Easing.InQuad
                        }

                        ResourceDelegate {
                            anchors.fill: parent
                            type: model ? model.type : RoomResource.Type_None
                            value: model ? model.value : undefined
                            onValueChanged: if(visible) flashAnimation.restart()
                            MouseArea{
                                anchors.fill: parent
                                onPressed: treeview.selection.setCurrentIndex(styleData.index, ItemSelectionModel.SelectCurrent);
                                onDoubleClicked: treeview.openDialog(model, styleData.index)
                            }
                        }
                        clip: true
                    }
                }
            }

            Connections { //Quick and dirty way to expand added items
                target: endpoints
                onRowsInserted: treeview.expand(endpoints.index(first, 0, parent))
            }

            Rectangle {
                id: msgView
                visible: !treeview.enabled
                color: "black"
                opacity: 0.8
                anchors.fill: parent

                Text {
                    anchors.fill: parent
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    color:"white"
                    font.pixelSize: 20
                    text: "Not connected";
                }
            }

            function openDialog(model ,index)
            {
                if(!model) return;
                if(model.isEndpoint === true) endpointDialog.openDialog(model)
                else resourceDialog.showEditorForModel(model, index)
            }
        }

        TextArea {
            id:logArea
            Layout.minimumHeight:50
            readOnly: true
            height: 100

            Connections {
                target:roomclient
                onLogOut: logArea.append(logString)
            }
        }
    }


    DialogResourceEditor {
        id: resourceDialog
    }

    DialogEndpointEditor {
        id: endpointDialog
    }
}
