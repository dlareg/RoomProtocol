import QtQuick 2.0
import RoomResource 1.0

Loader {
    id: resourceEditor

    property var value

    function setup(model) {

        switch(model.type * 1) {
        case RoomResource.Type_Bool:
            setSource("ResourceEditorBool.qml")
            break;

        case RoomResource.Type_String:
            setSource("ResourceEditorString.qml")
            break;

        case RoomResource.Type_Color:
            setSource("ResourceEditorColor.qml")
            break;
        case RoomResource.Type_Integer:
        default:
            value= model.value
            console.error("EDITOR NOT IMPLEMENTED FOR TYPE")
            break;
        }

        if(source) active=true
        if(item && item.setup) item.setup(model)
    }
}
