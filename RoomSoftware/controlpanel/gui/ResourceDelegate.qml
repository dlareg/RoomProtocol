import QtQuick 2.0
import RoomResource 1.0

Loader {
    id: delegateLoader

    property int type
    property var value

    source: {
        switch(type) {
        case RoomResource.Type_Integer:
        case RoomResource.Type_String:
            return "ResourceDelegateString.qml";
        case RoomResource.Type_Bool:
            return "ResourceDelegateBool.qml";
        case RoomResource.Type_Color:
            return "ResourceDelegateColor.qml";
        default:
            return "";
        }
    }
    clip: true
}
