import QtQuick 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: endpointDialog
    title: "Endpoint Editor"
    standardButtons:StandardButton.Ok | StandardButton.Reset

    property var currModel: undefined

    ColumnLayout {
        Text {
            id: endpointInfo
            text : endpointDialog.currModel
                    ? "<b>" + endpointDialog.currModel.label + "</b> (" + endpointDialog.currModel.location +")"
                    : ""
        }
    }

    function openDialog(model)
    {
        currModel = model
        open()
    }

    onReset:
    {
        endpointDialog.currModel.reset = 1;
        close()
    }
}
