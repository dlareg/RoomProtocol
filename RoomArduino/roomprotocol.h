#ifndef ROOMPROTOCOL_H
#define ROOMPROTOCOL_H

#define ROOM_BYTE_ESC               0xA0
#define ROOM_BYTE_START             0xA1
#define ROOM_BYTE_END               0xA2
#define ROOM_BYTE_SEP               0xA3
#define ROOM_BYTE_HEADER_SEP        0xA4
#define ROOM_BYTE_PING              0xA5
#define ROOM_BYTE_PONG              0xA6

#define ROOM_FRAME_VERB_INDEX       0
#define ROOM_FRAME_NOUN_INDEX       1
#define ROOM_FRAME_ARGS_INDEX       2

#define ROOM_RESERVED_BITS          0xF0
#define ROOM_RESERVED_VALUE         0xA0

#define ROOM_MAGIC_STRING           "ROOM02"

#define ROOM_DESCRIPTOR_ID          0x00
#define ROOM_DESC_MIN_VALID_FIELDS  2
#define ROOM_DESC_BITS_TYPE         0x0F
#define ROOM_DESC_BITS_CAPS         0xC0
#define ROOM_DESC_INDEX_MAGICSTRING 0
#define ROOM_DESC_INDEX_EPNAME      1
#define ROOM_DESC_RES_INDEX_FLAG    0
#define ROOM_DESC_RES_INDEX_LENGTH  1
#define ROOM_DESC_RES_INDEX_LABEL   2

#define ROOM_VERB_RESET_DONE        0x00
#define ROOM_VERB_DESC_REQ          0x01
#define ROOM_VERB_VALUE_CHANGED     0x02
#define ROOM_VERB_ACK               0x03
#define ROOM_VERB_NACK              0x04
#define ROOM_VERB_WRITE_EXEC        0x05
#define ROOM_VERB_READ              0x06

#define ROOM_VERB_ERR_UNKNOWN_VERB  0xFF
#define ROOM_VERB_ERR_UNKNOWN_NOUN  0xFE
#define ROOM_VERB_ERR_EMPTY_FRAME   0xFD
#define ROOM_VERB_ERR_BAD_FRAME     0xFC
#define ROOM_VERB_ERR_OVERFLOW      0xFB
#define ROOM_VERB_ERR_CANT_READ     0xFA
#define ROOM_VERB_ERR_CANT_WRITE    0xF9

#define ROOM_MAX_LENGTH             100
#define ROOM_MIN_LENGTH             2

#define ROOM_NODE_ENDPOINT_ID       0x00

#define ROOM_NODE_CMD_DISCOVERY     0x00
#define ROOM_NODE_CMD_RESET         0x01
#define TOOM_NODE_CMD_LOCATE        0x02

#define ROOM_NODE_DISCOVR_CACHE     0x00
#define ROOM_NODE_DISCOVR_SOFT      0x01
#define ROOM_NODE_DISCOVR_HARD      0x02

#define ROOM_TCP_DEFAULT_PORT       15685

#endif  // ROOMPROTOCOL_H
