#include "room.h"
#include "room_p.h"

#include <Arduino.h>

RoomLink::RoomLink(const char *endpointName):
    m_internals(RoomInternals(endpointName))
{

}

void RoomLink::processSerial(uint32_t delay)
{
    uint32_t startTime = millis();
    do {
        m_internals.processSerial();
    } while((millis() - startTime) < delay);
}

void RoomLink::begin()
{
    m_internals.begin();
}

//==============================================================================

RoomResource::RoomResource(RoomLink *link, const char* label, const Capabilities cap) :
    m_label(label),
    m_cap(cap),
    m_id(0),
    m_internals(&link->m_internals)
{
    m_type = Type_None;
    m_internals->registerRes(this);
}

bool RoomResource::canExec() const
{
    return m_cap & Cap_Exec;
}

bool RoomResource::canNotify() const
{
    return m_cap & Cap_Notif;
}

//==============================================================================

RoomBool::RoomBool(RoomLink* link, const char* label, const Capabilities cap, onBoolSetCallback callback = nullptr) :
    RoomResource(link, label, cap),
    m_callback(callback)
{
    m_value = false;
    m_type = Type_Bool;
}

bool RoomBool::get()
{
    return m_value;
}

void RoomBool::set(bool value)
{
    m_value = value;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_value);
}

bool RoomBool::parseData(const char *data, uint8_t length)
{
    if(length != 1 ) return false;
    m_value = (data[0] != 0x00);
    if(m_callback) m_callback(m_value);
    return true;
}

void RoomBool::sendValue()
{
    m_internals->pushPayloadByte((m_value == true) ? 0x01 : 0x00);
}

uint8_t RoomBool::getSize()
{
    return 1;
}

//==============================================================================

RoomColor::RoomColor(RoomLink *link, const char *label, const RoomResource::Capabilities cap, onColorSetCallBack callback) :
    RoomResource(link, label, cap),
    m_callback(callback),
    m_red(0x00), m_green(0x00), m_blue(0x00)
{
    m_type = Type_Color;
}

uint8_t RoomColor::red()
{
    return m_red;
}

uint8_t RoomColor::green()
{
    return m_green;
}

uint8_t RoomColor::blue()
{
    return m_blue;
}

void RoomColor::set(uint8_t red, uint8_t green, uint8_t blue)
{
    m_red = red;
    m_green = green;
    m_blue = blue;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setRed(uint8_t red)
{
    m_red = red;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setGreen(uint8_t green)
{
    m_green = green;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

void RoomColor::setBlue(uint8_t blue)
{
    m_blue = blue;
    m_internals->notifyResourceUpdated(this);
    if(m_callback) m_callback(m_red ,m_green, m_blue);
}

bool RoomColor::parseData(const char *data, uint8_t length)
{
    if(length != 3) return false;
    m_red   = data[0];
    m_green = data[1];
    m_blue  = data[2];
    if(m_callback) m_callback(m_red ,m_green, m_blue);
    return true;
}

void RoomColor::sendValue()
{
    m_internals->pushPayloadByte(m_red);
    m_internals->pushPayloadByte(m_green);
    m_internals->pushPayloadByte(m_blue);
}

uint8_t RoomColor::getSize()
{
    return 3;
}

