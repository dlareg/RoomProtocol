#include "room_p.h"
#include "room.h"

#include <Arduino.h>

static const char RoomInternals::s_roomMagicString[] PROGMEM = ROOM_MAGIC_STRING;

RoomInternals::RoomInternals(const char *endpointName):
    m_rxParserStage(Stage_Start),
    m_epName(endpointName),
    m_firstRes(nullptr),
    m_hasBegun(false)
{

}


void RoomInternals::registerRes(RoomResource *res)
{
    if(m_firstRes)
    {
        RoomResource *currRes = m_firstRes;
        while(currRes->m_next) currRes = currRes->m_next;
        currRes->m_next = res;
        res->m_id = currRes->m_id + 1;
    }
    else
    {
        m_firstRes = res;
        res->m_id = 1;
    }
}


void RoomInternals::notifyResourceUpdated(RoomResource* res)
{
    if(m_hasBegun) sendFrameResUpdate(res, ROOM_VERB_VALUE_CHANGED);
}

void RoomInternals::begin()
{
    sendFrameDescriptorAndValues(ROOM_VERB_RESET_DONE);
    m_hasBegun = true;
}

void RoomInternals::processSerial()
{
    for(uint8_t i = 0 ;             //Limit number of iteration ...
        (i < ROOM_MAX_LENGTH);// ... so flooding the link ...
        i++)                        // ... won't get us stuck here
    {
        if(!Serial.available()) break;
        uint8_t rxChar = Serial.read();
        bool payloadComplete = collectPayload(rxChar);
        if(payloadComplete) executePayload();
    }
}

bool RoomInternals::collectPayload(uint8_t c)
{
    //Frame start received (can happen at any time)
    if(c == ROOM_BYTE_START)
    {
        m_rxParserStage = Stage_Payload;
        m_rxBufferIndex = 0;
        m_isNextCharEscaped = false;
    }

    //Frame end received (can happen at any time)
    else if(c == ROOM_BYTE_END)
    {
        m_isNextCharEscaped = false;
        return true;
    }

    //Link test character
    else if(c  == ROOM_BYTE_PING)
    {
        Serial.write(ROOM_BYTE_PONG);
    }

    //Receiving payload
    else if(m_rxParserStage == Stage_Payload)
    {
        if(m_rxBufferIndex == ROOM_MAX_LENGTH)
        {
            m_rxParserStage = Stage_Overflow;
        }
        else if(c == ROOM_BYTE_ESC)
        {
            m_isNextCharEscaped = true;
        }
        else
        {
            m_rxBuffer[m_rxBufferIndex] = m_isNextCharEscaped ? (c ^ 0xFF) : c;
            m_rxBufferIndex++;
            m_isNextCharEscaped = false;
        }
    }

    return false;
}

void RoomInternals::executePayload()
{
    uint8_t verb = m_rxBuffer[ROOM_FRAME_VERB_INDEX];
    uint8_t noun = m_rxBuffer[ROOM_FRAME_NOUN_INDEX];

    bool isVerbKnown = ( verb == ROOM_VERB_READ ) || ( verb == ROOM_VERB_WRITE_EXEC ) ;

    //Errors
    if(m_rxParserStage == Stage_Start)          sendFrameError(ROOM_VERB_ERR_BAD_FRAME);
    else if(m_rxParserStage == Stage_Overflow)  sendFrameError(ROOM_VERB_ERR_OVERFLOW);
    else if(m_rxBufferIndex < ROOM_MIN_LENGTH)  sendFrameError(ROOM_VERB_ERR_EMPTY_FRAME);
    else if(!isVerbKnown)                       sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);

    //Descriptor
    else if(noun == ROOM_DESCRIPTOR_ID)
    {
        if(verb == ROOM_VERB_READ)              sendFrameDescriptorAndValues(ROOM_VERB_ACK);
        else if(verb == ROOM_VERB_WRITE_EXEC)   sendFrameError(ROOM_VERB_ERR_CANT_WRITE);
        else                                    sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);
    }

    //Resource
    else
    {
        RoomResource* res = getResFromId(noun);
        if(!res)                                sendFrameError(ROOM_VERB_ERR_UNKNOWN_NOUN);

        else if(verb == ROOM_VERB_WRITE_EXEC)
        {
            if(!res->canExec())                 sendFrameError(ROOM_VERB_ERR_CANT_WRITE);
            else if(res->parseData(&m_rxBuffer[ROOM_FRAME_ARGS_INDEX],
                                   m_rxBufferIndex - ROOM_FRAME_ARGS_INDEX))
                                                sendFrameAckAndValue(res);
            else                                sendFrameNack(res);
        }

        else if(verb == ROOM_VERB_READ)
        {
            if(!res->canNotify())               sendFrameError(ROOM_VERB_ERR_CANT_READ);
            else                                sendFrameAckAndValue(res);
        }

        else                                    sendFrameError(ROOM_VERB_ERR_UNKNOWN_VERB);
    }

    m_rxParserStage = Stage_Start;
}

void RoomInternals::sendFrameDescriptorAndValues(uint8_t resultCode)
{
    //Send descriptor
    Serial.write(ROOM_BYTE_START);
    pushPayloadByte(resultCode);
    pushPayloadByte(ROOM_DESCRIPTOR_ID);
    pushPayloadFlashString(s_roomMagicString);
    Serial.write(ROOM_BYTE_SEP);
    pushPayloadFlashString(m_epName);

    RoomResource* currRes = m_firstRes;
    while(currRes)
    {
        Serial.write(ROOM_BYTE_SEP);
        pushPayloadByte(currRes->m_cap | currRes->m_type);
        pushPayloadByte(currRes->getSize());
        pushPayloadFlashString(currRes->m_label);
        currRes = currRes->m_next;
    }
    Serial.write(ROOM_BYTE_END);

    //Send values
    const uint8_t valueResultCode = (resultCode == ROOM_VERB_RESET_DONE) ?
                ROOM_VERB_RESET_DONE : ROOM_VERB_DESC_REQ;
    for(RoomResource* res = m_firstRes; res != nullptr; res = res->m_next)
    {
        sendFrameResUpdate(res, valueResultCode);
    }
}

void RoomInternals::sendFrameResUpdate(RoomResource *res, uint8_t resultCode)
{
    if( !(res->m_cap & RoomResource::Cap_Notif)) return;

    Serial.write(ROOM_BYTE_START);
    pushPayloadByte(resultCode);
    pushPayloadByte(res->m_id);
    res->sendValue();
    Serial.write(ROOM_BYTE_END);
}

void RoomInternals::sendFrameError(uint8_t result)
{
    Serial.write(ROOM_BYTE_START);
    pushPayloadByte(result);
    Serial.write(ROOM_BYTE_END);
}

void RoomInternals::sendFrameNack(RoomResource *res)
{
    Serial.write(ROOM_BYTE_START);
    pushPayloadByte(ROOM_VERB_NACK);
    pushPayloadByte(res->m_id);
    Serial.write(ROOM_BYTE_END);
}

void RoomInternals::sendFrameAckAndValue(RoomResource *res)
{
    Serial.write(ROOM_BYTE_START);
    pushPayloadByte(ROOM_VERB_ACK);
    pushPayloadByte(res->m_id);
    if(res->canNotify())res->sendValue();
    Serial.write(ROOM_BYTE_END);
}


void RoomInternals::pushPayloadFlashString(const uint8_t *string)
{
    while(1)
    {
        uint8_t c = pgm_read_byte_near(string);
        if(c == '\0') break;
        pushPayloadByte(c);
        string++;
    }
}


void RoomInternals::pushPayloadByte(uint8_t c)
{
    if ((c & ROOM_RESERVED_BITS) == ROOM_RESERVED_VALUE)
    {
        Serial.write(ROOM_BYTE_ESC);
        Serial.write(c ^ 0xFF);
    }
    else
    {
        Serial.write(c);
    }
}

RoomResource* RoomInternals::getResFromId(uint8_t id)
{

    for(RoomResource* currRes = m_firstRes;
        currRes != nullptr;
        currRes = currRes->m_next)
    {
        if(currRes->m_id == id) return currRes;
    }
    return nullptr;
}
