#ifndef ROOM_H
#define ROOM_H

#include "room_p.h"

#include <stdint.h>

class RoomInternals;

class RoomLink
{
    friend class RoomResource;
public:
    RoomLink(const char* endpointName);
    void processSerial(uint32_t delay = 0);
    void begin();
private:
    RoomInternals m_internals;
};

//==============================================================================

class RoomResource
{
    friend class RoomInternals;
public:
    enum Capabilities
    {
        Cap_None = 0x00,
        Cap_Notif = 0x80,
        Cap_Exec = 0x40,
        Cap_NotifExec = Cap_Notif | Cap_Exec
    };

protected:
    RoomResource(RoomLink* link, const char* label, const Capabilities cap);

    virtual bool parseData(const char* data, uint8_t length) = 0;
    virtual void sendValue() = 0;
    virtual uint8_t getSize() = 0;

    enum Type
    {
        Type_None = 0x00,
        Type_Bool = 0x01,
        Type_String = 0x02,
        Type_Integer = 0x03,
        Type_Color = 0x04,
    };

    Type m_type;

    RoomInternals* m_internals;

    bool canExec() const;
    bool canNotify() const;

private:
    const char *m_label;
    const Capabilities m_cap;
    uint8_t m_id;
    RoomResource *m_next;
};

//==============================================================================

class RoomBool : public RoomResource
{
public:
    typedef void (*onBoolSetCallback)(const bool);

    RoomBool(RoomLink* link, const char* label, const Capabilities cap, onBoolSetCallback callback = nullptr);

    bool get();
    void set(bool value);

protected:
    bool parseData(const char *data, uint8_t length) override;
    void sendValue() override;
    uint8_t getSize() override;


private:
    bool m_value;
    onBoolSetCallback m_callback;
};

//==============================================================================

template<uint8_t Size=0>class RoomString : public RoomResource
{
public:
    typedef void (*onStringSetCallback)(const char*);

    RoomString(RoomLink* link, const char* label, const Capabilities cap, onStringSetCallback callback = nullptr) :
        RoomResource(link, label, cap),
        m_callback(callback)
    {
        for(unsigned int i = 0 ; i < Size ; i++) m_data[i] = '\0';
        m_type=Type_String;
    }

    const unsigned char* get()
    {
        return m_data;
    }

    void set(unsigned char* str)
    {
        for(uint8_t i = 0 ; i < Size ; i++)
        {
            m_data[i] = str[i];
            if(str[i] == '\0') break;
        }
        m_internals->notifyResourceUpdated(this);
        if(m_callback) m_callback(m_data);
    }

    void setFromFlash(unsigned char* str)
    {
        for(uint8_t i = 0 ; i < Size ; i++)
        {
            unsigned char c = pgm_read_byte_near(&str[i]);
            m_data[i] = c;
            if(c == '\0') break;
        }
        m_internals->notifyResourceUpdated(this);
        if(m_callback) m_callback(m_data);
    }

    bool compareToFlash(unsigned char* flashStr) const
    {
        for(uint8_t i = 0 ; i < Size ; i++)
        {
            unsigned char correctChar = pgm_read_byte_near(&flashStr[i]);
            if(correctChar != m_data[i]) return false;
            if(correctChar == '\0') return true;
        }
        return false;
    }

    void fill(unsigned char c)
    {
        for(uint8_t i = 0 ; i < Size ; i++)
        {
            m_data[i] = c;
        }
        m_internals->notifyResourceUpdated(this);
        if(m_callback) m_callback(m_data);
    }

    unsigned char getChar(uint8_t i)
    {
        return (i < Size) ? m_data[i] : 0x00;
    }

    void setChar(uint8_t i, unsigned char c)
    {
        if(i >= Size) return;
        m_data[i] = c;
        m_internals->notifyResourceUpdated(this);
        if(m_callback) m_callback(m_data);
    }


protected:
    bool parseData(const char *data, uint8_t length) override
    {
        if(length > Size) return false;

        for(uint8_t i = 0 ; i < Size ; i++)
        {
            m_data[i] = (i < length) ? data[i] : '\0';
        }

        if(m_callback) m_callback(m_data);

        return true;
    }

    void sendValue() override
    {
        for(uint8_t i = 0 ; i < Size ; i++ )
        {
            m_internals->pushPayloadByte(m_data[i]);
            if(m_data[i] == '\0') break;
        }
    }

    uint8_t getSize() override
    {
        return (uint8_t)Size;
    }

private:
    unsigned char m_data[Size];
    onStringSetCallback m_callback;
};

//==============================================================================

class RoomColor : public RoomResource
{
public:
    typedef void (*onColorSetCallBack)(const uint8_t, const uint8_t, const uint8_t);

    RoomColor(RoomLink* link, const char* label, const Capabilities cap, onColorSetCallBack callback = nullptr);

    uint8_t red();
    uint8_t green();
    uint8_t blue();

    void set(uint8_t red, uint8_t green, uint8_t blue);
    void setRed(uint8_t red);
    void setGreen(uint8_t green);
    void setBlue(uint8_t blue);

protected:
    bool parseData(const char *data, uint8_t length) override;
    void sendValue() override;
    uint8_t getSize() override;


private:
    uint8_t m_red;
    uint8_t m_green;
    uint8_t m_blue;
    onColorSetCallBack m_callback;
};

#endif // ROOM_H
