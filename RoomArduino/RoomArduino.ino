/*
Project:	Gourmandise
Board:
MCU:
FRENQUENCY:	8000000

Created using Qt Creator
*/
#ifndef ARDUINO_H
#define ARDUINO_H
#include <Arduino.h>
#endif

#include <avr/pgmspace.h>
#include <avr/wdt.h>
#include <LiquidCrystal.h>
#include "room.h"

const char name[] PROGMEM = "ROOM DEMO DEVICE";
const char flag1Str[] PROGMEM = "Booleen";
const char colorStr[] PROGMEM = "Couleur";
const char string1Str[] PROGMEM = "String";
const char ledStr[] PROGMEM = "LED";


RoomLink roomLink(name);

RoomBool flag1(&roomLink, flag1Str, RoomResource::Cap_NotifExec);
RoomColor color1(&roomLink, colorStr, RoomResource::Cap_NotifExec);
RoomString<20> string1(&roomLink, string1Str, RoomResource::Cap_NotifExec);
RoomBool led1(&roomLink, ledStr, RoomResource::Cap_NotifExec, &writeLed);


void writeLed(bool ledValue)
{
    digitalWrite(13,ledValue ? HIGH : LOW);
}



void setup() {
    wdt_enable(WDTO_4S);
    wdt_reset();

    Serial.begin(115200); // Initialize serial communications with the PC

    pinMode(13,OUTPUT);
    digitalWrite(13,LOW);

    color1.set(random(255),random(255),random(255));
    string1.fill( 65 + random(50) );

    roomLink.begin();

    wdt_reset();

}


void loop()
{
    flag1.set(!flag1.get());

    roomLink.processSerial(1000);
    wdt_reset();
}
