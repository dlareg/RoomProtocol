# RoomArduino

TODO

# RoomSoftware

Build RoomControlPanel :
```
$ cd RoomProtocol/RoomSoftware/controlpanel
$ qmake
$ make
$ ./RoomControlPanel
```

To use with remote host : compile and start RoomNode on host computer, and RoomControlPanel on local computer

On local computer : start RoomControlPanel with host address and port as argument : 
```
./RoomControlPanel XXX.XXX.XXX.XXX YYYYY
```
(Port can be omitted, default port will be used)

On host computer :
```
$ cd RoomProtocol/RoomSoftware/node
$ qmake
$ make
$ ./RoomNode YYYYY
```
with YYYYY the port to use 
